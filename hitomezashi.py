import numpy as np
import matplotlib.pyplot as plt
from os import system, name

##def Hitomezashi_Patterns(X, Y, borders=True, c=None):
##    '''code to generate the plot, starting from binary arrays-like X and Y
##    toggle "borders" for borders and "c" for a monochromatic plot
##    X = [1, 0, 1, 1, 1, 0, 0, 1] and such'''
##    lx = len(X)-1
##    ly = len(Y)-1
##    
##    plt.figure()
##    
##    if borders: # frame
##        plt.plot([0, lx], [0, 0], color=c) 
##        plt.plot([0, lx], [ly, ly], color=c)
##        plt.plot([0, 0], [0, lx], color=c)
##        plt.plot([ly, ly], [0, lx], color=c)
##        
##    for row, y in enumerate(Y):
##        for j in range(abs(y-1), lx, 2):
##            plt.plot([j, j+1], [row, row], color=c)
##            
##    for col, x in enumerate(Y):
##        for j in range(abs(x-1), ly, 2):
##            plt.plot([col, col], [j, j+1], color=c)
##            
##    plt.yticks(range(ly+1), [str(i) for i in Y]) # final plt touches
##    plt.xticks(range(lx+1), [str(i) for i in X])
##    plt.title("Hitomezashi Stitch Patterns")
##    plt.show()


def Hitomezashi_Patterns(X, Y, borders=True, c=None):
    '''code to generate the plot, starting from binary arrays-like X and Y
    toggle "borders" for borders and "c" for a monochromatic plot'''
    plt.figure()
    if borders: # frame
        plt.plot([0, len(X)-1], [0, 0], color=c) 
        plt.plot([0, len(X)-1], [len(Y)-1, len(Y)-1], color=c)
        plt.plot([0, 0], [0, len(X)-1], color=c)
        plt.plot([len(Y)-1, len(Y)-1], [0, len(X)-1], color=c)
    row = 0 # horizontal lines
    for y in Y:
        j = 0 if y==1 else 1
        while j < len(X)-1:
            plt.plot([j, j+1], [row, row], color=c)
            j += 2
        row += 1
    col = 0 # vertical lines
    for x in X:
        j = 0 if x==1 else 1
        while j < len(Y)-1:
            plt.plot([col, col], [j, j+1], color=c)
            j += 2
        col += 1
    plt.yticks(range(len(Y)), [str(i) for i in Y]) # final plt touches
    plt.xticks(range(len(X)), [str(i) for i in X])
    plt.title("Hitomezashi Stitch Patterns")
    plt.show()


def ex():
    rand = np.random.RandomState()
    while 1:
        tot = rand.randint(3, 41)
        p = rand.uniform()
        
        axis = rand.choice([0, 1], p=[p, 1-p], size=(2, tot))
        X = axis[0]
        Y = axis[1]
        
        print("Size: %d"%tot)
        print("Probs: %.4f, %.4f"%(p, 1-p))
        Hitomezashi_Patterns(X, Y, borders=rand.choice([True, False]), c=rand.choice([None, "k"]))


def main():

    rand = np.random.RandomState(seed=4628)
    tot = 30
    P = None #[0, 1]
    axis = rand.choice([0, 1], p=P, size=(2, tot))
    X = axis[0]
    Y = axis[1]

    Hitomezashi_Patterns(X, Y, c="k")

    ex()

        

if __name__=="__main__":
    main()


    

